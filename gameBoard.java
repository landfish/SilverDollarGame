/** Made by Jeffrey Ladish as part of CSF 2015 Winter quarter
 * This class stores the board information
 * All the methods are static
*/

import java.util.Vector;
import java.util.ArrayList;
import java.util.*;
import java.lang.*;
import javax.swing.*;

public class gameBoard {

  //This ArrayList will store the values of the gameboard
  //Each value will be either a 0 for empty or 1 for dollar
  public static ArrayList<Integer> boardValues;

  //The board length can be changed and the program will
  //still run correctly
  public static int boardLength = 15;

  //The dollarNum can also be changed without messing
  //up the program
  static int dollarNum = 5;

  static Random rand = new Random();

  //Is a constructor even necessary if I'm only using static methods?
  public gameBoard() {
  }

  //Sets up ArrayList boardValues with initial values of 0, then
  //and randomly adds dollars (represented by an int value of 1)
  //The number of dollars is determined by dollarNum
  public static void initializeBoard() {
    boardValues = new ArrayList<Integer>();
    for (int i = 0; i < boardLength; i++) {
      boardValues.add(0);
    }
    for (int i = 0; i < dollarNum; i++) {
      int randint = rand.nextInt(boardLength);

      //ensures that a value of 1 will only replace a value of 0
      //so the proper number of 1s will be placed
      while (boardValues.get(randint) == 1) {
        randint = rand.nextInt(boardLength);
      }
      boardValues.set(randint,1);
    }
  }

  //returns true if there are no longer legal  moves
  public static boolean checkFinished() {
    for (int i = 1; i < boardValues.size(); i++) {
      if(checkMove(i,i-1)) {
        return false;
      }
    }
    return true;
  }

  //Returns false if move is illegal
  //Checks to see whether piece in position int a can legally be
  //moved to piece in position int b
  public static boolean checkMove(int a, int b) {
    if (a < 0 || b < 0) {
      //System.out.println("Checkmove is false: Lower than zero");
      return false;
    }
    if (a > boardValues.size()-1 || b > boardValues.size()-1){
      //System.out.println("Checkmove is false: Greater than array");
      return false;
    }
    if (boardValues.get(a) != 1) {
      //System.out.println("Checkmove is false: First selection is not equal to 1");
      return false;
    }
    if (boardValues.get(b) != 0) {
      //System.out.println("Checkmove is false: First selection is not equal to 0");
      return false;
    }
    if (firstLeft(a) > b) {
      //System.out.println("Checkmove is false: Second selection is too far to the left");
      return false;
    }
    else {
      return true;
    }
  }

  //Returns the value of the first piece to the left of the specified piece
  public static int firstLeft(int a) {
    int piecePos = a;
    for (int i = piecePos-1; i >= 0; i--) {
      if (boardValues.get(i) == 1) {
        return i;
      }
    }
    return -1;
  }

  //Method for moving dollars
  //Sets a to 0 and b to 1
  public static void move (int a, int b) {
    boardValues.set(a,0);
    boardValues.set(b,1);
  }

  //Prints board, adding an extra space for the 10th position and after
  //so the board will align with the reference board in printBoardRef()
  public static String printBoard() {
    String boardString = "|";
    for (int i = 0; i < boardValues.size(); i++) {
      int tempVar = boardValues.get(i);
      if (tempVar == 0) {
        if (i < 10) {
          boardString += " |";
        }
        else {
          boardString += "  |";
        }
      }
      if (tempVar == 1) {
        if (i < 10) {
          boardString += "O|";
        }
        else {
          boardString += " O|";
        }
      }
    }
    return boardString;
  }
  
  //Returns a board with reference numbers for user reference      
  public static String printBoardRef() {
    String boardString = "|";
    for (int i = 0; i < boardValues.size(); i++) {
      boardString += i + "|";
    }
    return boardString;
  }

  //A test method to return an integer at a position
  public static int getPos(int a) {
    return boardValues.get(a);
  }

  //A test method to return the size of the ArrayList
  public static int getSize() {
    return boardValues.size();
  }
}
