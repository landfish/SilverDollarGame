/** SilverDollar program created by Jeffrey Ladish for CSF winter quarter
 ** This class handles user input and prints the board and text to the
 ** terminal.
*/

import java.util.Scanner;

public class dollarDriver {

  public static void main(String[] args) {
  
    //Initializes board, which is stored as an ArrayList of ones and zeros
    gameBoard.initializeBoard();

    Scanner scan = new Scanner(System.in);
    int moveFrom;
    int moveTo;

    //Main game loop runs until there are no more positions left to move
    while (!gameBoard.checkFinished()) {

      //Intializes the variables as false so the loop for each player will run
      boolean player1LegMove = false;
      boolean player2LegMove = false;
      
      //This value is changed if player 1 wins so the game will end
      boolean gameEnd = false;

      //Loop for player 1 turn
      while (player1LegMove == false) {
        //Board and reference board are printed
        System.out.println(gameBoard.printBoard());
        System.out.println(gameBoard.printBoardRef());
        System.out.println();

        //Player is prompted seperately enter the position of the piece
        //they want to move and where they want to move it
        System.out.println("Player one, specify the position number of the piece you would like to move");
        moveFrom = scan.nextInt();
        System.out.println("Player one, specify the position number of the empty space you would like to move to");
        moveTo = scan.nextInt();

        //Checks if user input represents legal move
        if (!gameBoard.checkMove(moveFrom,moveTo)) {
          System.out.println("Not a legal move. Try again.");
        }
        else {
          gameBoard.move(moveFrom,moveTo);
          player1LegMove = true;
        }
        //Checks if there are remaining legal moves
        if (gameBoard.checkFinished()) {
          System.out.println("Player 1 Wins!");
          System.out.println(gameBoard.printBoard());
          //variable changed so player 2 loop will not initialize
          gameEnd = true;
        }
      }

      //Loop for player 2 turn
      //Identical to player 1 loop except for gameEnd check
      while (player2LegMove == false && gameEnd == false) {
        //Board and reference board are printed
        System.out.println(gameBoard.printBoard());
        System.out.println(gameBoard.printBoardRef());
        System.out.println();
        //Player is prompted seperately enter the position of the piece
        //they want to move and where they want to move it
        System.out.println("Player two, specify the position number of the piece you would like to move");
        moveFrom = scan.nextInt();

        System.out.println("Player two, specify the position number of the empty space you would like to move to");
        moveTo = scan.nextInt();

        //Checks if user input represents legal move
        if (!gameBoard.checkMove(moveFrom,moveTo)) {
          System.out.println("Not a legal move. Try again.");
        }
        else {
          gameBoard.move(moveFrom,moveTo);
          player2LegMove = true;
        //Checks if there are remaining legal moves
        }
        if (gameBoard.checkFinished()) {
          System.out.println("Player 2 Wins!");
          System.out.println(gameBoard.printBoard());
        }
      }
    }
  }
}

    
